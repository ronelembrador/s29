// require directive tells us to load the express module
const express = require('express');

// creating a server using express;
const app = express();

// port
const port = 4000;

// middlewares
app.use(express.json())
	// allows app to read a json data
app.use(express.urlencoded({extended: true}));
	// allows app to read data from forms
	// by default, information received from the url can only be received as string or an array
	// with extended: true, this allows to receive information in other data types such as objects.

// mock database

let users = [
	{
		email: "nezukoKamado@gmail.com",
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "tenjiroKamado@gmail.com",
		username: "gonpanchiro",
		password: "iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: false
	}

]

let loggedUser;

// GET method
app.get('/', (req,res) =>{
	res.send('Hello World')
})

// app - server
// get - HTPP method
// '/' - route name or endpoint
// (req, res) - request and response - will handle the requests and responses
// res.send - combines writeHead() and end(), used to send response to our client

app.get('/hello', (req,res) =>{
	res.send('Hello from Batch 131')
})


// POST method
app.post('/', (req,res) =>{
	console.log(req.body);
		// use console.log to check what's inside the request body.
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}. I could be described as ${req.body.description}}`)

})

app.post('/users/register', (req,res) =>{
	console.log(req.body);
	
	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered.`)
})


app.post('/users/login', (req,res) =>{
	console.log(req.body);
	
	let foundUser = users.find((user) => {

		return user.username === req.body.username && user.password === req.body.password
	});

	if(foundUser !== undefined){

		let foundUserIndex = users.findIndex((user) => {

			return user.username === foundUser.username
		});

		foundUser.index = foundUserIndex;

		loggedUser = foundUser

		console.log(foundUser)

		res.send('Thank you for logging in.')
	} else {
		loggedUser = foundUser;

		res.send('Login failed, wrong credentials.')
	}
})

// Change-Password Route

app.put('/users/change-password', (req, res) => {

	// store the message that will be sent back to our client
	let message;

	// will loop through all the "users" array.
	for(let i = 0; i < users.length; i++){

		// if the username provided in the request is the same with the username in the loop
		if(req.body.username === users[i].username){

			// change the password of the user found in the loop by the requested password in the body by the client
			users[i].password = req.body.password;

			// send a message to the client
			message = `User ${req.body.username}'s password has been changed.`;

			// break the loop once a user matches the username provided in the client
			break;
			
		// if no user was found
		} else {

			// changes the message to be sent as a response
			message = `User not found.`
		}
	}

	// response that will be sent to our client.
	res.send(message)

})

// Activity

// 1. Create a GET route that will access the /home route that will print out a simple message
app.get('/home', (req,res) => {
	res.send('Welcome to the home page')
})

// 2. Process a GET request at the /home route using postman

// 3. Create a GET route that will access the /users route that will retrieve all the users in the mock database
app.get('/users', (req,res) => {
	let message = ``;
	
	users.forEach(name => {
		message += `User: ${name.username} `
	})

	res.send(message)
})

// 4. Process a GET request at the /users route using postman

// 5. Create a DELETE route that will access the /delete-user route that will remove a user from the mock database

app.delete('/delete-user', (req,res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			users.splice(i, 1);

			message = `User ${req.body.username} has been deleted.`;

			break;
			
		} else {

			message = `There is no such user.`
		}
	}

	res.send(message)

})

// 6. Process a DELETE request at the /delete-user route using postman

// 7. Export the Postman collection and save it inside the root folder of our application


// listen to the port and returning message in the terminal
app.listen(port, () => console.log(`Server is running at port ${port}`));